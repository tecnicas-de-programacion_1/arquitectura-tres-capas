package presentacion;

import logica.ConsultaGptDTO;
import logica.ConsultaApiDTO;
import logica.PaisesDTO;
import com.fasterxml.jackson.databind.JsonNode;
import java.awt.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.event.*;
import java.net.URI;
import javax.swing.*;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Consultaindicedesigualdad {
    private JPanel panelDesigualdadpais;
    private JTextField textCountry;
    private JPanel tittleMain;
    private JPanel selectCountry;
    private JButton moreButton;
    private JButton comidaTipicaButton;
    private JButton breveHistoriaButton;
    private JButton noticiasButton;
    private JButton moreinfoQuestionButton;
    private JPanel firstPanel;
    private JPanel viewFlag;
    private JPanel consultCountryGPT;
    private JPanel consultAPI;
    private JPanel bringCapital;
    private JPanel bringGNI;
    private JTextField txtInfoGNI;
    private JPanel bringPopulation;
    private JTextField txtInfoPopulation;
    private JPanel secondPanel;
    private JPanel questionsPanel;
    private JPanel developmentPanels;
    private JButton searchButton;
    private JLabel pais;
    private JLabel lblCapital;
    private JLabel lblPoblacion;
    private JLabel lblGni;
    private JLabel lblRespuestaChat;
    private JLabel lblNombrepais;
    private JLabel lblImagenes;
    private JLabel lblBandera;
    private JTextField txtInfoCapital;
    private JLabel lblInfoApi;
    private JPanel Panelprueba;
    private JButton btPreguntasFrecuentes;
    private JLabel lblBanderaPreguntas;
    private JButton buscarButton;
    private JLabel lblEscudo;
    private JTabbedPane tabbedPane1;
    private JButton buttonVolver;
    private JTextArea textAreaGPTPais;
    private JButton animalesButton;
    private JButton dislexiaButton;
    private JLabel lblImagenDesigualdad;


    public Consultaindicedesigualdad() {
        tittleMain.setToolTipText("Este es el título");
        selectCountry.setToolTipText("Este es el panel para buscar el país");
        lblNombrepais.setVisible(false);
        lblCapital.setVisible(false);
        txtInfoCapital.setVisible(false);
        lblPoblacion.setVisible(false);
        txtInfoPopulation.setVisible(false);
        lblGni.setVisible(false);
        txtInfoGNI.setVisible(false);
        moreinfoQuestionButton.setVisible(false);
        lblInfoApi.setVisible(false);
        btPreguntasFrecuentes.setVisible(false);

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            botonEnter();
            }
        });

        moreinfoQuestionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int guardarPDF = JOptionPane.showConfirmDialog(null, "¿Quieres guardar la información?", "Guardar información", JOptionPane.YES_NO_OPTION);

                if (guardarPDF == JOptionPane.YES_OPTION) {

                    ConsultaApiDTO consultaApiDTO = new ConsultaApiDTO();
                    consultaApiDTO.generarArchivo(txtInfoCapital.getText(), txtInfoPopulation.getText(), txtInfoGNI.getText(), textCountry.getText());
                    JOptionPane.showMessageDialog(null, "Información guardada");
                } else {
                    JOptionPane.showMessageDialog(null, "Cancelado");
                }
            }
        });

        lblNombrepais.addComponentListener(new ComponentAdapter() {
        });
        moreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                lblNombrepais.setVisible(true);
                lblCapital.setVisible(true);
                txtInfoCapital.setVisible(true);
                lblPoblacion.setVisible(true);
                txtInfoPopulation.setVisible(true);
                lblGni.setVisible(true);
                txtInfoGNI.setVisible(true);
                moreinfoQuestionButton.setVisible(true);
                lblInfoApi.setVisible(true);
                btPreguntasFrecuentes.setVisible(true);
                moreinfoQuestionButton.setVisible(true);

                String nombrePais = textCountry.getText();
                System.out.println(nombrePais);
                lblNombrepais.setText(nombrePais);

                ConsultaApiDTO consultaApiDTO = new ConsultaApiDTO();

                String url = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/World_Map_Gini_coefficient_with_legend_2.png/1200px-World_Map_Gini_coefficient_with_legend_2.png";
                Image imagen = consultaApiDTO.obtenerImagenDesdeURL(url);
                lblImagenDesigualdad = new JLabel(new ImageIcon(imagen));
                JFrame frame = new JFrame("Imagen de desigualdad");
                frame.getContentPane().add(lblImagenDesigualdad, BorderLayout.CENTER);
                frame.pack();
                frame.setVisible(true);

                try {
                    String response = consultaApiDTO.getPais(nombrePais);
                    ObjectMapper objectMapper = new ObjectMapper();
                    JsonNode json = objectMapper.readTree(response);
                    String year = "2019";
                        String capital = json.get(0).get("capital").get(0).asText();
                        int poblacion = json.get(0).get("population").asInt();
                        double gini = 0;
                        try {
                            gini = json.get(0).get("gini").get(year).asDouble();
                            txtInfoGNI.setText(String.valueOf(gini));
                        } catch (Exception ex) {
                            System.out.println("error pais no trae GINI");
                            String mensaje = "El país no cuenta con GINI actualizado del 2019";
                            txtInfoGNI.setText(mensaje);
                        }
                        if (year != year) {
                            JOptionPane.showMessageDialog(panelDesigualdadpais, "El valor obtenido no es para el año " + year);
                        }
                        String flags = json.get(0).get("flags").get("png").asText();
                        lblBandera.setIcon(consultaApiDTO.procesarImagenes(flags));
                        txtInfoCapital.setText(capital);
                        txtInfoPopulation.setText(String.valueOf(poblacion));

                        PaisesDTO paisesDTO = new PaisesDTO();
                        if (paisesDTO.guardar(nombrePais, capital, poblacion, gini)) {
                            lblInfoApi.setText("Se guardó información con éxito");
                        } else {
                            lblInfoApi.setText("Error al gurdar información");
                        }
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(panelDesigualdadpais, ex, "Información BD", JOptionPane.ERROR_MESSAGE);

                }
            }

        });

        btPreguntasFrecuentes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String nombrePais = textCountry.getText();
                ConsultaApiDTO consultaApiDTO = new ConsultaApiDTO();

                try {
                    String response = consultaApiDTO.getPais(nombrePais);
                    ObjectMapper objectMapper = new ObjectMapper();
                    JsonNode json = objectMapper.readTree(response);

                    String flags = json.get(0).get("flags").get("png").asText();
                    String coatOfArms = json.get(0).get("coatOfArms").get("png").asText();
                    lblBanderaPreguntas.setIcon(consultaApiDTO.procesarImagenes(flags));
                    lblEscudo.setIcon(consultaApiDTO.procesarImagenes(coatOfArms));

                    tabbedPane1.setSelectedIndex(1);

                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        comidaTipicaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConsultaGptDTO consultaGPTDTO = new ConsultaGptDTO("¿Cuál es la Comida típica de " + textCountry.getText() + "?");
                String respuesta = consultaGPTDTO.consultarChatGPT();
                System.out.println(respuesta);
                String mensaje = "<html><body style='width: 300px'>" + respuesta.replace("\n", "<br>") + "</body></html>";
                JOptionPane.showMessageDialog(null, mensaje, "Información del país", JOptionPane.INFORMATION_MESSAGE);

            }
        });

        animalesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConsultaGptDTO consultaGPTDTO = new ConsultaGptDTO("¿Cuáles son los animales en peligro de extinción que se encuentran en " + textCountry.getText() + "?");
                String respuesta = consultaGPTDTO.consultarChatGPT();
                System.out.println(respuesta);
                String mensaje = "<html><body style='width: 300px'>" + respuesta.replace("\n", "<br>") + "</body></html>";
                JOptionPane.showMessageDialog(null, mensaje, "Información del país", JOptionPane.INFORMATION_MESSAGE);

            }
        });


        noticiasButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConsultaGptDTO consultaGPTDTO = new ConsultaGptDTO("¿Cuál es la noticia más importante en " + textCountry.getText() + " en los últimos 2 años?");
                String respuesta = consultaGPTDTO.consultarChatGPT();
                System.out.println(respuesta);
                String mensaje = "<html><body style='width: 300px'>" + respuesta.replace("\n", "<br>") + "</body></html>";
                JOptionPane.showMessageDialog(null, mensaje, "Información del país", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        breveHistoriaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConsultaGptDTO consultaGPTDTO = new ConsultaGptDTO("Digame una breve historia de " + textCountry.getText() + ". Tenga en cuenta solo los datos más relevantes");
                String respuesta = consultaGPTDTO.consultarChatGPT();
                System.out.println(respuesta);
                String mensaje = "<html><body style='width: 300px'>" + respuesta.replace("\n", "<br>") + "</body></html>";
                JOptionPane.showMessageDialog(null, mensaje, "Información del país", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        dislexiaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConsultaGptDTO consultaGPTDTO = new ConsultaGptDTO("¿Cuál es el porcentaje de disléxicos en el país " + textCountry.getText() + "?");
                String respuesta = consultaGPTDTO.consultarChatGPT();
                System.out.println(respuesta);
                String mensaje = "<html><body style='width: 300px'>" + respuesta.replace("\n", "<br>") + "</body></html>";
                JOptionPane.showMessageDialog(null, mensaje, "Información del país", JOptionPane.INFORMATION_MESSAGE);
            }

        });
        buscarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                abrirHipervinculo();
            }
        });
        buttonVolver.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabbedPane1.setSelectedIndex(0);
            }
        });
        textCountry.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                int key = e.getKeyChar();
                if (key == 10) {
                    botonEnter();
                    System.out.println("ENTER pressed");
                }
            }
        });
    }

    public void abrirHipervinculo() {
        String url = "https://nmpq.typeform.com/to/Vu5CNa?typeform-source=irunmimundoalreves.com";
        try {
            Desktop.getDesktop().browse(new URI(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void botonEnter(){
        ConsultaGptDTO consultaGPTDTO = new ConsultaGptDTO("Digame tres caracteristicas del pais " + textCountry.getText());
        String respuesta = consultaGPTDTO.consultarChatGPT();
        System.out.println(respuesta);
        textAreaGPTPais.setLineWrap(true);
        textAreaGPTPais.setPreferredSize(new Dimension(300, 70)); // Ajusta los valores de ancho y alto según tus necesidades
        textAreaGPTPais.setText(respuesta.strip());
    }

    public JPanel getSecondPanel() {
        return this.secondPanel;

    }

    public JPanel getPanelDesigualdadpais() {
        return this.panelDesigualdadpais;

    }

    public JPanel getQuestionsPanel() {

        return this.questionsPanel;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }



}