package persistencia;

import java.sql.*;

public class PaisesDAO {

    private Connection connection;

    public PaisesDAO(){
        Conexion conexion = new Conexion();
        this.connection = conexion.getConnection();
        this.crearTabla();
    }

    private void crearTabla(){
        try{
            Statement statement = this.connection.createStatement();
            statement.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS paises (id INTEGER PRIMARY KEY, nombre_pais TEXT, capital TEXT , poblacion INT, gni FLOAT ) ");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public boolean crear(String nombrePais, String capital, int poblation, double gini){
        try {
            PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "INSERT INTO paises(nombre_pais, capital, poblacion, gni) VALUES (?, ?, ?, ?) "
            );
            preparedStatement.setString(1, nombrePais);
            preparedStatement.setString(2, capital);
            preparedStatement.setString(3, String.valueOf(poblation));
            preparedStatement.setString(4, String.valueOf(gini));
            preparedStatement.execute();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }

    }

    public ResultSet listar(){
        ResultSet resultSet = null;
        try{
            Statement statement = this.connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM usuarios");
            return resultSet;

        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

}