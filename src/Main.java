import presentacion.Consultaindicedesigualdad;

import javax.swing.JFrame;

public class Main {
    public static void main(String[] args) {

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Consultaindicedesigualdad consultaindiceDesigualdad = new Consultaindicedesigualdad();
        frame.setContentPane(consultaindiceDesigualdad.getPanelDesigualdadpais());
        frame.setTitle("Índice de desigualdad");
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
        System.out.println("Prueba de funcionamiento");
    }
}