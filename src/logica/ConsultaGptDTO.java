package logica;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
public class ConsultaGptDTO {

    private String apiUrl = "https://api.openai.com/v1/completions";
    private String openaiApiKey = " ";

    private String ask;

    private String text;
    public ConsultaGptDTO(String ask){

        this.ask = ask;
    }

    public String consultarChatGPT(){

        try{

            URL url = new URL(this.apiUrl);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + this.openaiApiKey);

            connection.setDoOutput(true);
            System.out.println(this.ask);
           // String jsonInputString = "{\"model\":\"text-davinci-003\",\"prompt\":\"digame en solo tres palabras qué es colombia?\",\"max_tokens\":70,\"temperature\":0.8}";
            String jsonInputString = "{\"model\":\"text-davinci-003\",\"prompt\":\" " + this.ask + "\",\"max_tokens\":100,\"temperature\":0.8}";


            try(OutputStream outputStream = connection.getOutputStream()){
                outputStream.write(jsonInputString.getBytes());
                outputStream.flush();

            }
            StringBuilder response = new StringBuilder();
            try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
                String inputLine;
                while((inputLine = bufferedReader.readLine()) !=null){
                    response.append(inputLine);
                }
            }

            ObjectMapper objectMapper =  new ObjectMapper();
            JsonNode jsonResponse = objectMapper.readTree(response.toString());
            JsonNode choices =jsonResponse.get("choices");

            for (JsonNode choice : choices){

                text = choice.get("text").asText();
                System.out.println(text);
            }

        } catch (Exception e){
            System.out.println(e);
        }
        return text;
    }
}
