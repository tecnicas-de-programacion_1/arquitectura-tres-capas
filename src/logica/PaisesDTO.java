package logica;

import persistencia.PaisesDAO;

public class PaisesDTO {
    private String nombrePais;
    private String capital;
    private int poblation;
    private double gini;

    public boolean guardar(String nombrePais, String capital, int poblation, double gini){

        this.nombrePais = nombrePais;
        this.capital = capital;
        this.poblation = poblation;
        this.gini = gini;

        PaisesDAO paisesDAO = new PaisesDAO();
        boolean respuesta = paisesDAO.crear(this.nombrePais, this.capital, this.poblation, this.gini);
        return respuesta;
    }

}
