package logica;

import persistencia.ArchivosDAO;

import javax.swing.*;
import java.awt.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;


public class ConsultaApiDTO {
    private static final String BASE_URL = "https://restcountries.com/v3.1/name/";

    public String getPais(String nombrePais) throws IOException {

        URL url = new URL(BASE_URL + nombrePais.replace(" ", "%20"));
        System.out.println(BASE_URL + nombrePais);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            JOptionPane.showMessageDialog(null, "No se encontró información del país: " + nombrePais, "Error", JOptionPane.ERROR_MESSAGE);
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();

    }

    public ImageIcon procesarImagenes(String urlImagen) {
        try {
            URL url = new URL(urlImagen);
            ImageIcon imagen = new ImageIcon(url);
            int ancho = 600;
            int alto = 300;
            Image img = imagen.getImage().getScaledInstance(ancho, alto, Image.SCALE_SMOOTH);
            ImageIcon imagenEscalada = new ImageIcon(img);
            return imagenEscalada;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Image obtenerImagenDesdeURL(String url) {
        try {
            ImageIcon imageIcon = new ImageIcon(new URL(url));
            Image image = imageIcon.getImage();
            Image scaledImage = image.getScaledInstance(300, 200, Image.SCALE_SMOOTH);
            return scaledImage;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    public void generarArchivo(String capital, String poblacion, String gni, String country) {
        ArchivosDAO archivosDAO = new ArchivosDAO();
        archivosDAO.guardar(capital, poblacion, gni, country);
    }

}